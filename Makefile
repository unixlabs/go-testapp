all: clean prepare build container
all+push: clean prepare build container push
version := $(shell cat version.txt)
clean:
	rm -f server
prepare:
	go fmt server.go
build:
	CGO_ENABLED=0 GOOS=linux GOARCH=amd64  go build -ldflags "-X 'main.VersionString=${version}' -w -s" -a -installsuffix cgo -o server server.go
container:
	docker build -t registry.gitlab.com/unixlabs/go-testapp:${version} .
push:
	docker push registry.gitlab.com/unixlabs/go-testapp:${version}
