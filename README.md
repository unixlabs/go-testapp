# go-testapp
Test App written in Golang
# run
```
docker run -d -p 8080:8080 registry.gitlab.com/unixlabs/go-testapp:3
```
# build
```
make all+push
```
