package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
)

var (
	VersionString string = "development"
)

func handler(w http.ResponseWriter, r *http.Request) {
	hostname, _ := os.Hostname()
	fmt.Fprintf(w, "Hi there, here is %s in version %s!\n", hostname, VersionString)
}

func main() {
	http.HandleFunc("/", handler)
	log.Fatal(http.ListenAndServe(":8080", nil))
}
